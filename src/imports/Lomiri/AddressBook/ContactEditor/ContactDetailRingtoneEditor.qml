/*
 * Copyright (C) 2023 Ubports Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import QtContacts 5.0
import Lomiri.Content 1.3
import Lomiri.Components.Popups 1.3

import Lomiri.Contacts 0.1
import Lomiri.AddressBook.Base 0.1

//style
import Lomiri.Components.Themes.Ambiance 0.1

ContactDetailBase {
    id: root

    detailType: ContactDetail.Ringtone

    activeFocusOnTab: true
    implicitHeight: contents.implicitHeight
    visible: !empty || newRingtoneRequested

    property bool newRingtoneRequested: false
    property bool empty: true
    property Loader overlayLoader: null

    onDetailChanged: {
        if (detail) {
            populateValues(detail)
        }
    }

    function open() {
        populateValues(contact.ringtone)
        newRingtoneRequested = true
    }

    function isEmpty() {
        return empty
    }

    function populateValues(contactDetail) {
        ringTones.clear()

        if (contactDetail) {
            const audioUrl = contactDetail.value(Ringtone.AudioRingtoneUrl)
            const videoUrl = contactDetail.value(Ringtone.VideoRingtoneUrl)
            const messageUrl = contactDetail.value(Ringtone.VibrationRingtoneUrl)

            root.empty = !audioUrl // && !messageUrl && !videoUrl

            ringTones.append({"type": Ringtone.AudioRingtoneUrl, "label": i18n.dtr("lomiri-addressbook-app", "Incoming call"), "value": audioUrl ? audioUrl.toString() : ""})
            //disable Ringtone.VideoRingtoneUrl and Ringtone.VibrationRingtoneUrl for now
            //ringTones.append({"type": Ringtone.VideoRingtoneUrl, "label": i18n.dtr("lomiri-addressbook-app", "Incoming video"), "value": audioUrl ? videoUrl.toString() : ""})
            //ringTones.append({"type": Ringtone.VibrationRingtoneUrl, "label": i18n.dtr("lomiri-addressbook-app", "Incoming message"), "value": messageUrl ? messageUrl.toString() : ""})
        }
    }

    function closeOverlay() {
        overlayLoader.source = "";
        overlayLoader.active = false;
    }

    function openSoundList(currentIndex, ringtoneType) {
        overlayLoader.onLoaded.connect(function() {
            var item = overlayLoader.item
            item.currentRingtoneType = ringtoneType
            item.currentSoundFile = ringTones.get(currentIndex).value
            item.closeRequested.connect(closeOverlay)
            item.soundSelected.connect(function(newSound) {
              ringTones.setProperty(currentIndex, "value", newSound)
            })

        })

        overlayLoader.source = Qt.resolvedUrl("ContactDetailRingtoneSoundSelector.qml")
        overlayLoader.active = true
    }

    function save() {
        var detailchanged = false
        if (root.contact && root.contact.ringtone) {
            var detail = root.contact.ringtone

            for( var i = 0; i < ringTones.count; i++ ) {
                var ringTone = ringTones.get(i);
                console.log('save detail:', ringTone.value, detail.value(ringTone.type))
                if (ringTone.value !== detail.value(ringTone.type)) {
                    detail.setValue(ringTone.type, ringTone.value)
                    detailchanged = true
                }
            }
        }
        return detailchanged
    }

    function soundName(soundPath) {

        if (soundPath.length > 0) {
            /* The display name starts with an uppercase char, and replace special chars with spaces */
            var title = soundPath.split('/').pop().split('.').slice(0,-1).join(" ").replace(/[._-]/g, " ")
            if (title.length > 0) {
                return title[0].toUpperCase() + title.slice(1)
            }
        }

        return i18n.tr("Default sound")
    }

    ListModel {
        id: ringTones
    }

    Column {
        id: contents
        spacing: units.gu(1)
        width: parent.width

        ContactDetailTitle {
            id: header
            title: i18n.dtr("lomiri-addressbook-app", "Custom ringtone")
        }

        Repeater {
            model: ringTones

            Column {

                anchors {
                    margins: units.gu(2)
                    left: parent.left
                    right: parent.right
                }

                Item {
                    id: ringToneFieldItem

                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    height: childrenRect.height

                    TextField {
                        id: input
                        width: parent.width
                        text:  soundName(value)

                        placeholderText: i18n.dtr("lomiri-addressbook-app", "Select a custom sound")
                        style: TextFieldStyle {
                            overlaySpacing: 0
                            frameSpacing: 0
                            background: Item {}
                        }

                        ProgressionSlot {
                            visible: !clearButton.visible
                            anchors.right: parent.right
                            anchors.verticalCenter: parent.verticalCenter
                        }

                    }

                    Label {
                        id: typeLabel
                        anchors.top: input.bottom
                        //verticalAlignment: Text.AlignVCenter
                        elide: Text.ElideRight
                        text: label
                        // style
                        fontSize: "small"
                        opacity: 0.8
                    }

                    Item {
                        anchors.top: typeLabel.bottom
                        height: units.gu(2)
                    }

                    MouseArea {
                        anchors.fill: parent
                        onClicked: openSoundList(index, type)
                    }

                    AbstractButton {
                        id: clearButton
                        objectName: "clear_button"
                        activeFocusOnPress: false
                        activeFocusOnTab: false
                        visible: value.length > 0
                        anchors {
                            top: input.top
                            right: input.right
                            margins: units.gu(0.5)
                            verticalCenter: input.verticalCenter

                        }
                        z: 100
                        width: visible ? icon.width : 0

                        Icon {
                            id: icon
                            anchors.verticalCenter: parent.verticalCenter
                            width: units.gu(2.5)
                            height: width
                            // use icon from icon-theme
                            name: "edit-clear"
                        }

                        onClicked: {
                            ringTones.setProperty(index, "value", "")
                        }
                    }
                }
            }
        }
    }

    Connections {
        target: addNewFieldButton
        function onFieldSelected(detailType, fieldName, qmlTypeName) {
            if (detailType === root.detailType) {
                root.open()
            }
        }
    }
}
