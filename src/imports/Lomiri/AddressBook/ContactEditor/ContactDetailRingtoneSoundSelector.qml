/*
 * Copyright (C) 2023 Ubports Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.0
import QtQuick.Layouts 1.12
import Lomiri.Components 1.3
import Lomiri.Components.Popups 1.3
import Lomiri.Content 1.3
import QtMultimedia 5.6
import Lomiri.Contacts 0.1

Page {
    id: root
    objectName: "contactSoundsPage"

    property var activeTransfer
    property string currentSoundFile: ""
    property alias currentRingtoneType: ringToneModel.ringToneType

    onCurrentSoundFileChanged: soundSelected(currentSoundFile)

    signal soundSelected(string file)
    signal closeRequested()

    function requestFileDelete(path) {
        var dialog = PopupUtils.open(deleteConfirm, root)
        dialog.canceled.connect(function() {
            PopupUtils.close(dialog)
        });
        dialog.accepted.connect(function() {
            ringToneModel.removeFile(path)
            PopupUtils.close(dialog)
        });
    }

    function onImportedFile(uri) {
        const newFile = uri.toString().replace("file:///", "/")
        ringToneModel.addFile(newFile);

        root.currentSoundFile = newFile
        soundList.currentIndex = ringToneModel.getIndex(root.currentSoundFile);
        soundList.positionViewAtIndex(soundList.currentIndex, ListView.Center)
        soundEffect.play()
    }

    header: PageHeader {
        title: i18n.dtr("lomiri-addressbook-app", "Select a custom sound")
        leadingActionBar {
            actions: [
                Action {
                    text: i18n.tr("back")
                    iconName: "back"
                    onTriggered: {
                        root.closeRequested()
                    }
                }
            ]
        }

        trailingActionBar {
            actions: [
                Action {
                    text: i18n.tr("import")
                    iconName: "import"
                    onTriggered: {
                        PopupUtils.open(pickerDialog, null)
                    }
                }
            ]
        }
    }

    Audio {
        id: soundEffect
        audioRole: MediaPlayer.NotificationRole
        source: currentSoundFile
    }

    RingtoneModel {
        id: ringToneModel
    }

    Rectangle {
        anchors.fill: parent
        color: theme.palette.normal.background
    }

    Label {
        anchors.centerIn: parent
        text: i18n.tr("No sounds found")
        visible: soundList.count === 0
    }

    ColumnLayout {
        id: column
        anchors {
            top: header.bottom
            left: parent.left
            right: parent.right
            bottom: stopItem.top

        }
        ListView {
            id: soundList
            Layout.fillHeight: true
            Layout.fillWidth: true
            currentIndex: -1
            model: ringToneModel
            onCountChanged: {
                if (count >0) {
                    soundList.currentIndex = ringToneModel.getIndex(root.currentSoundFile);
                    soundList.positionViewAtIndex(soundList.currentIndex, ListView.Center)
                }
            }

            clip: true
            section {
                property: "readOnly"
                criteria: ViewSection.FullString
                labelPositioning: ViewSection.InlineLabels
                delegate: Rectangle {
                    anchors {
                        left: parent.left
                        right: parent.right
                        margins: units.gu(2)
                    }

                    color: Theme.palette.normal.background
                    height: units.gu(6)
                    Label {
                        id: title

                        anchors.fill: parent
                        verticalAlignment: Text.AlignVCenter
                        height: units.gu(3)
                        font.bold: true
                        text: section == "true" ? i18n.tr("System sounds:") : i18n.tr("Custom sounds:")
                    }
                }
            }

            delegate: ListItem {
                id: soundItem
                height: layout.height + (divider.visible ? divider.height : 0)
                selected: soundList.currentIndex === index
                swipeEnabled: !readOnly
                leadingActions: ListItemActions {
                    actions: [
                        Action {
                            iconName: "delete"
                            onTriggered: root.requestFileDelete(path)
                        }
                    ]
                }
                ListItemLayout {
                    id: layout
                    title.text: name;

                    Icon {
                        name: "tick"
                        visible: soundItem.selected
                        SlotsLayout.position: SlotsLayout.Trailing;
                        width: units.gu(2)
                    }
                }
                onClicked: {
                    soundList.currentIndex = index
                    root.currentSoundFile = path
                    soundEffect.play()
                }
            }
        }
    }

    ListItem {
        id: stopItem
        anchors.bottom: parent.bottom
        enabled: soundEffect.playbackState == Audio.PlayingState
        visible: enabled
        AbstractButton {
            id: stopButton
            anchors.verticalCenter: parent.verticalCenter
            anchors.horizontalCenter: parent.horizontalCenter
            focus: false
            width: height
            height: units.gu(4)
            visible: stopItem.enabled

            Rectangle {
                anchors.fill: parent
                radius: width * 0.5
                border.color: theme.palette.normal.raisedText
                border.width: 1
            }

            Rectangle {
                width: parent.height * 0.4
                height: width
                smooth: true
                anchors {
                    verticalCenter: parent.verticalCenter
                    horizontalCenter: parent.horizontalCenter
                }
                color: theme.palette.normal.raisedText
            }
            onClicked: soundEffect.stop()
        }

        Rectangle {
            anchors.fill: parent
            z: parent.z - 1
            visible: stopItem.visible
            color: Theme.palette.normal.background
        }
    }

    Component {
        id: pickerDialog

        Item {
            id: picker

            property var activeTransfer: null
            signal avatarReceived(string avatarUrl)
            signal destruction()

            anchors.fill: parent

            function hide() {
                picker.visible = false;
            }

            function show() {
                picker.visible = true;
            }

            ContentTransferHint {
                anchors.fill: parent
                activeTransfer: picker.activeTransfer
            }

            ContentPeerPicker {
                id: peerPicker

                anchors.fill: parent
                handler: ContentHandler.Source
                contentType: ContentType.Music
                showTitle: true

                onPeerSelected: {
                    peer.selectionType = ContentTransfer.Single
                    picker.activeTransfer = peer.request()
                }

                onCancelPressed: PopupUtils.close(picker);
            }

            Connections {
                id: contentHubConnection

                property var allowedExtensions :["wav","ogg","mp3"]

                target: picker.activeTransfer

                function onStateChanged() {
                    if (activeTransfer.state === ContentTransfer.Charged) {
                        if (activeTransfer.items.length > 0) {

                            var customSoundPath = ringToneModel.soundPath(currentRingtoneType)

                            var item = activeTransfer.items[0];
                            const isMusicFile = allowedExtensions.some((ext) => item.url.toString().endsWith(ext));
                            if (!isMusicFile) {
                                PopupUtils.open(unsupportedFile, root)
                                activeTransfer.finalize()
                                return;
                            }

                            var toneUri;
                            if (item.move(customSoundPath)) {
                                toneUri = item.url;
                            } else {
                                PopupUtils.open(importError, root)
                                activeTransfer.finalize()
                                return;
                            }

                            console.log("moved file to:", toneUri)
                            PopupUtils.close(picker)

                            root.onImportedFile(toneUri);
                        }
                        activeTransfer.finalize()
                    }
                }
            }
        }
    }

    Component {
        id: deleteConfirm

        Dialog {
            id: dialog

            property int threadCount: 0

            title: i18n.tr("Delete file")
            text: i18n.tr("Are you sure you want to delete this file ?")

            signal accepted()
            signal canceled()

            Column {
                anchors.left: parent.left
                anchors.right: parent.right
                spacing: units.gu(2)
                Row {
                    anchors.horizontalCenter: parent.horizontalCenter
                    spacing: units.gu(4)
                    Button {
                        text: i18n.tr("Cancel")
                        onClicked: dialog.canceled()
                    }
                    Button {
                        text: i18n.tr("Delete")
                        color: theme.palette.normal.negative
                        onClicked: dialog.accepted()
                    }
                }
            }
        }
    }

    Component {
        id: unsupportedFile
        Dialog {
            id: dialog

            title: i18n.tr("Unsupported file")

            text: i18n.tr("Sorry, only ogg, mp3 and wav file format is supported")

            Column {
                anchors.left: parent.left
                anchors.right: parent.right
                spacing: units.gu(2)
                Row {
                    anchors.horizontalCenter: parent.horizontalCenter
                    spacing: units.gu(4)
                    Button {
                        text: i18n.tr("ok")
                        onClicked: dialog.hide()
                    }
                }
            }
        }
    }

    Component {
        id: importError
        Dialog {
            id: dialog

            title: i18n.tr("Import error")

            text: i18n.tr("Sorry, an error occurred while importing")

            Column {
                anchors.left: parent.left
                anchors.right: parent.right
                spacing: units.gu(2)
                Row {
                    anchors.horizontalCenter: parent.horizontalCenter
                    spacing: units.gu(4)
                    Button {
                        text: i18n.tr("ok")
                        onClicked: dialog.hide()
                    }
                }
            }
        }
    }

    Component.onDestruction: {
        if (soundEffect.playbackState === Audio.PlayingState) {
            soundEffect.stop()
        }
    }
}

