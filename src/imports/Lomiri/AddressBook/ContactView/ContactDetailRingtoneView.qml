/*
 * Copyright (C) 2023 Ubports Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

import QtQuick 2.4
import Lomiri.Components 1.3
import Lomiri.Components.ListItems 1.3
import QtContacts 5.0

import Lomiri.AddressBook.Base 0.1

import Lomiri.Components.Themes.Ambiance 0.1

ContactDetailBase {
    id: root

    detailType: ContactDetail.Ringtone

    activeFocusOnTab: false
    implicitHeight: empty ? 0 : contents.implicitHeight
    visible: implicitHeight > 0

    property bool empty: true

    onDetailChanged: {
        if (detail) {
            populateValues(detail)
        }
    }

    function populateValues(detail) {
        ringTones.clear()

        if (detail) {
            const audioUrl = detail.value(Ringtone.AudioRingtoneUrl)
            const videoUrl = detail.value(Ringtone.VideoRingtoneUrl)
            const messageUrl = detail.value(Ringtone.VibrationRingtoneUrl)

            root.empty = !audioUrl //&& !videoUrl && !messageUrl
            if (audioUrl) {
                ringTones.append({"type": Ringtone.AudioRingtoneUrl, "label": i18n.dtr("lomiri-addressbook-app", "Incoming call"), "value": audioUrl ? audioUrl.toString() : ""})
            }
            /*
            if (videoUrl) {
                ringTones.append({"type": Ringtone.VideoRingtoneUrl, "label": i18n.dtr("lomiri-addressbook-app", "Incoming video"), "value": audioUrl ? videoUrl.toString() : ""})
            }
            if (messageUrl) {
                ringTones.append({"type": Ringtone.VibrationRingtoneUrl, "label": i18n.dtr("lomiri-addressbook-app", "Incoming message"), "value": messageUrl ? messageUrl.toString() : ""})
            }
            */
        }
    }

    function soundName(soundPath) {

        if (soundPath.length > 0) {
            /* The display name starts with an uppercase char, and replace special chars with spaces */
            var title = soundPath.split('/').pop().split('.').slice(0,-1).join(" ").replace(/[._-]/g, " ")
            if (title.length > 0) {
                return title[0].toUpperCase() + title.slice(1)
            }
        }

        return i18n.tr("Default sound")
    }

    ListModel {
        id: ringTones
    }

    Column {
        id: contents
        spacing: units.gu(1)
        width: parent.width

        ContactDetailTitle {
            id: header
            title: i18n.dtr("lomiri-addressbook-app", "Custom ringtones")
        }

        Repeater {
            model: ringTones

            Column {

                anchors {
                    margins: units.gu(2)
                    left: parent.left
                    right: parent.right
                }

                Label {
                    id: input
                    text:  soundName(value)
                }

                Label {
                    id: typeLabel

                    verticalAlignment: Text.AlignVCenter
                    elide: Text.ElideRight
                    text: label
                    // style
                    fontSize: "small"
                    opacity: 0.8
                }
            }
        }
    }

    Connections {
        target: contact
        function onContactChanged() {
            if (contact) {
                populateValues(contact.detail(root.detailType))
            }
        }
    }
}
