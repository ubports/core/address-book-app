/*
 * Copyright (C) 2023 Ubports Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "ringtonemodel.h"

#include <QDir>
#include <QFileInfo>
#include <QStandardPaths>
#include <QContactRingtone>
#include <QDebug>

#define DEFAULT_SOUND_DIR "sounds/lomiri/ringtones/"
#define DEFAULT_NOTIFICATION_DIR "sounds/lomiri/notifications/"
#define CONTACT_SOUND_DIR "sounds/ringtones"

bool sortRingtones(const Ringtone &s1, const Ringtone &s2)
{
    return s1.name() < s2.name();
}

QString fileName(const QString &path)
{
    QFileInfo fileInfo(path);
    QString name = fileInfo.baseName().replace(QRegExp("[._-]"), " ");
    return name.at(0).toUpper() + name.mid(1);
}

QList<Ringtone> ringtonesFromDir(const QString &dirString)
{
    QList<Ringtone> ringtones;
    QDir soundsDir(dirString);
    bool readOnly = false;
    if (soundsDir.isRelative()) {
        readOnly = true;
        QString path = QStandardPaths::locate(
            QStandardPaths::GenericDataLocation, dirString,
            QStandardPaths::LocateDirectory
        );
        if (path.isEmpty()) {
            return ringtones;
        }
        soundsDir = QDir(path);
    }


    if (soundsDir.exists())
    {
        soundsDir.setFilter(QDir::Files | QDir::NoSymLinks);

        for (uint i=0; i < soundsDir.count(); i++) {
            ringtones << Ringtone(fileName(soundsDir[i]),soundsDir.absoluteFilePath(soundsDir[i]), readOnly );
        }
    }
    return ringtones;
}

Ringtone::Ringtone(const QString &name, const QString &path, bool readonly)
    : mName(name), mPath(path), mReadonly(readonly)
{
}

QString Ringtone::name() const
{
    return mName;
}

QString Ringtone::path() const
{
    return mPath;
}

bool Ringtone::readonly() const
{
    return mReadonly;
}

RingtoneModel::RingtoneModel(QObject *parent) : QAbstractListModel(parent)
{

    QString genericDataLocation = QStandardPaths::writableLocation(QStandardPaths::GenericDataLocation);

    mAudioRingtonePath = QString(genericDataLocation + "/" + CONTACT_SOUND_DIR + "/audio");
    QDir dir(mAudioRingtonePath);
    if (!dir.exists())
        dir.mkpath(".");

    mVideoRingtonePath = QString(genericDataLocation + "/" + CONTACT_SOUND_DIR + "/video");
    QDir dirv(mVideoRingtonePath);
    if (!dirv.exists())
        dirv.mkpath(".");

    mVibrationRingtonePath = QString(genericDataLocation + "/" + CONTACT_SOUND_DIR + "/notification");
    QDir dirm(mVibrationRingtonePath);
    if (!dirm.exists())
        dirm.mkpath(".");

    connect(this, SIGNAL(ringtoneTypeChanged()), SLOT(populatesData()));

}

void RingtoneModel::populatesData()
{
    QtContacts::QContactRingtone::RingtoneField type = (QtContacts::QContactRingtone::RingtoneField) mRingToneType;
    QStringList sounds;
    QStringList soundDirs;
    if (QtContacts::QContactRingtone::RingtoneField::FieldAudioRingtoneUrl == type) {
        soundDirs << DEFAULT_SOUND_DIR << mAudioRingtonePath;
    } else if (QtContacts::QContactRingtone::RingtoneField::FieldVideoRingtoneUrl == type) {
        soundDirs << DEFAULT_SOUND_DIR << mVideoRingtonePath;
    } else if (QtContacts::QContactRingtone::RingtoneField::FieldVibrationRingtoneUrl == type) {
        soundDirs << DEFAULT_NOTIFICATION_DIR << mVibrationRingtonePath;
    }

    qDebug() << "Ringtone paths:" << soundDirs;

    beginResetModel();
    mRingtones.clear();
    endResetModel();

    QList<Ringtone> ringtones;
    for (int i = 0; i < soundDirs.size(); ++i) {
        QList<Ringtone> tmpRingtones = ringtonesFromDir(soundDirs[i]);
        qSort(tmpRingtones.begin(), tmpRingtones.end(), sortRingtones);
        ringtones << tmpRingtones;
    }

    beginInsertRows(QModelIndex(), 0, ringtones.count() - 1);
    mRingtones.append(ringtones);
    endInsertRows();

}

void RingtoneModel::setRingToneType(int ringToneType)
{
    mRingToneType = ringToneType;
    Q_EMIT ringtoneTypeChanged();
}

int RingtoneModel::getIndex(const QString &path)
{
    for (int i = 0; i < mRingtones.count(); i++) {
        if (mRingtones[i].path() == path) {
            return i;
        }
    }
    return -1;
}

QString RingtoneModel::soundPath(int ringtoneField)
{
    QtContacts::QContactRingtone::RingtoneField ringToneType = (QtContacts::QContactRingtone::RingtoneField) ringtoneField;

    if (QtContacts::QContactRingtone::RingtoneField::FieldAudioRingtoneUrl == ringToneType) {
        return mAudioRingtonePath;
    } else if (QtContacts::QContactRingtone::RingtoneField::FieldVideoRingtoneUrl == ringToneType) {
        return mVideoRingtonePath;
    } else if (QtContacts::QContactRingtone::RingtoneField::FieldVibrationRingtoneUrl == ringToneType) {
        return mVibrationRingtonePath;
    } else {
        return "";
    }
}

void RingtoneModel::addFile(const QString &file)
{
    beginInsertRows(QModelIndex(), 0, 0);
    mRingtones << Ringtone( fileName(file), file, false);
    qSort(mRingtones.begin(), mRingtones.end(), sortRingtones);
    endInsertRows();
}

void RingtoneModel::removeFile(const QString &path)
{
    int index = getIndex(path);
    if (index > -1) {
        QFile file (path);
        beginRemoveRows(QModelIndex(), index, index);
        if (file.remove()) {
            mRingtones.removeAt(index);
        } else {
            qWarning() << "error when trying to remove" << path;
        }

        endRemoveRows();
    }
}

QHash<int, QByteArray> RingtoneModel::roleNames() const
{
    static QHash<int, QByteArray> roles;
    if (roles.isEmpty()) {
        roles[Name] = "name";
        roles[Path] = "path";
        roles[ReadOnly] = "readOnly";
    }
    return roles;
}

int RingtoneModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent);
    return mRingtones.count();
}

QVariant RingtoneModel::data(const QModelIndex & index, int role) const
{
    if (index.row() < 0 || index.row() >= mRingtones.count())
        return QVariant();

    const Ringtone &ringtone = mRingtones[index.row()];
    if (role == Name)
        return QVariant::fromValue(ringtone.name());
    else if (role == Path)
        return QVariant::fromValue(ringtone.path());
    else if (role == ReadOnly) {
        return QVariant::fromValue(ringtone.readonly());
    } else {
        return QVariant();
    }
}
