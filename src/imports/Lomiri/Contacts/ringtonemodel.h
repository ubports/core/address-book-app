/*
 * Copyright (C) 2023 Ubports Foundation
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef RINGTONEMODEL_H
#define RINGTONEMODEL_H

#include <QtCore/QAbstractListModel>
#include <QObject>

class Ringtone
{
public:
    Ringtone(const QString &name, const QString &path, bool readonly);

    QString name() const;
    QString path() const;
    bool readonly() const;

private:
    QString mName;
    QString mPath;
    bool mReadonly;
};

class RingtoneModel : public QAbstractListModel
{
    Q_OBJECT
    Q_PROPERTY(int ringToneType WRITE setRingToneType)


public:
    RingtoneModel(QObject* parent=0);

    enum Roles {
        Name = Qt::UserRole + 1,
        Path,
        ReadOnly
    };

    void setRingToneType(int ringToneType);
    Q_INVOKABLE int getIndex(const QString &path);
    Q_INVOKABLE QString soundPath(int ringtoneField);
    Q_INVOKABLE void addFile(const QString &file);
    Q_INVOKABLE void removeFile(const QString &path);

    // reimplemented from QAbstractListModel
    QHash<int, QByteArray> roleNames() const;
    int rowCount(const QModelIndex& parent=QModelIndex()) const;
    QVariant data(const QModelIndex& index, int role) const;

Q_SIGNALS:
    void ringtoneTypeChanged();

private Q_SLOTS:
    void populatesData();

private:

    QList<Ringtone> mRingtones;
    int mRingToneType;
    QString mAudioRingtonePath;
    QString mVibrationRingtonePath;
    QString mVideoRingtonePath;
};



#endif // RINGTONEMODEL_H
